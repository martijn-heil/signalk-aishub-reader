import 'xml2js';
import 'moment';
import 'urlencode';
import 'zip';
import * as fetch from 'node-fetch';

function deg2rad(deg) {
  return deg / (180 / Math.PI)
}

function knotsToMetersPerSecond(kts) {
  return kts * 1.852 * 1000 / 3600;
}

function navstat2string(navstat) {
  let states = [
      "Under way using engine",
      "At anchor",
      "Not under command",
      "Restricted manoeuverability",
      "Constrained by her draught",
      "Moored",
      "Aground",
      "Engaged in Fishing",
      "Under way sailing",
      "Invalid: Reserved for future amendment of Navigational Status for HSC",
      "Invalid: Reserved for future amendment of Navigational Status for WIG",
      "Invalid: Reserved for future use (11)",
      "Invalid: Reserved for future use (12)",
      "Invalid: Reserved for future use (13)",
      "AIS-SART is active",
      null // Not defined (default)
  ];
  return states[navstat];
}

function aisshiptype2object(shiptype) {

}

function positionQuality2String(a) {
    if (a == 0) return "low accuracy";
    else if (a == 1) return "high accuracy";
}

function aisGnssFixType2String(fixType) {
  let table = [
    null,
    "GPS",
    "GLONASS",
    "Combined GPS/GLONASS",
    "Loran-C",
    "Chayka",
    "Integrated navigation system",
    "Surveyed",
    "Galileo",
  ];
  return table[fixType];
}

function getAppropiateContext(mmsi) {
  let mmsiString = mmsi;
  if (typeof mmsiString !== "string") { mmsiString = mmiString.toString(); }

  let mmsiType = parseInt(mmsiString.charAt(0))
  if (mmsiType === NaN) { return null; }
      
  if (mmsiType > 2 && mmsiType <= 7) { context = `vessels.urn:mrn:imo:mmsi:${mmsi}`; } // Individual ships
  else if (mmsiType == 1) { context = `aircraft.urn:mrn:imo:mmsi:${mmsi}`; }
  else if (mmsiType == 9) {
    if (mmsi.charAt(1) == "9") { context = `aton.urn:mrn:imo:mmsi:${mmsi}`; } // Aids to navigation
    else if (mmsi.charAt(1) == "8") { context = `vessels.urn:mrn:imo:mmsi:${mmsi}`;} // Craft associated with a parent ship
    else if (mmsi.charAt(1) == "7") { context = `sar.urn:mrn:imo:mmsi:${mmsi}`; } // Various SAR devices. (AIS-SART, EPIRBs, MOB devices)
    else { return null; }
  } else if (mmsiType == 8) { return null; }
  else { return null; }
}

async function AisHubXmlToDeltas(xmlStr) {
  try {
    let parser = new xml2js.Parser();
    let data = await parser.parseStringPromise(xmlStr);
    console.log(JSON.stringify(data, null, 2));
    let vessels = data.VESSELS.vessel.map(it => it["$"])
    return vessels.map(vessel => {
      let name = vessel.NAME;
      let callsignVhf = vessel.CALLSIGN;
      let navstat = navstat2string(parseInt(vessel.NAVSTAT, 10));
      let imoNumber = vessel.IMO;
      let draft = vessel.DRAUGHT;
      if (typeof draft != "number") { draft = parseFloat(draft); }
      if (draft === NaN) { draft = null; }
      else { draft = draft / 10; }
      
      let fromBow = parseFloat(vessel.A);
      let fromStern = parseFloat(vessel.B);

      if (name === "") { name = null; }
      if (callsignVhf === "") { callsign = null; }
      if (navstat === NaN) { navstat = null; }
      if (imoNumber === "" || imoNumber === "0") { imoNumber = null; }
      
      if (fromBow === NaN) { fromBow = null; }
      if (fromStern === NaN) { fromStern = null; }

      let length = null;
      if (fromBow != null && fromStern != null) { length = fromBow + fromStern; }

      let context = null;
      let mmsi = vessel.MMSI;
      let context = getAppropiateContext(mmsi);
      if (context == null) {
        console.error(`Could not get appropiate context for MMSI ${mmsi}.`);
        return;
      }

      return {
        "context": context,
        "updates": [
          {
            "source": {
              "label": "AISHub",
              "type": "AISHub",
            },
            "timestamp": moment.unix(parseInt(vessel.TIME, 10)).toISOString(),
            "values": [
              {
                "path": "name",
                "value": name,
              },
              {
                "path": "registrations.imo",
                "value": imoNumber,
              },
              {
                "path": "communication.callsignVhf",
                "value": callsignVhf,
              },
              {
                "path": "navigation.state",
                "value": navstat,
              },
              {
                "path": "navigation.position",
                "value": {
                  "longitude": vessel.LONGITUDE / 600000,
                  "latitude": vessel.LATITUDE / 600000,
                  "altitude": null,
                },
              },
              {
                "path": "navigation.courseOverGroundTrue",
                "value": (vessel.COG == 3600) ? null : deg2rad(vessel.COG / 10),
              },
              {
                "path": "navigation.speedOverGround",
                "value": (vessel.SOG == 104.2) ? null : knotsToMetersPerSecond(vessel.SOG / 10)
              },
              {
                "path": "navigation.headingTrue",
                "value": (vessel.HEADING != 511) ? deg2rad(vessel.HEADING) : null,
              },
              // TODO rate of turn
              {
                "path": "navigation.gnss.type",
                "value": aisGnssFixType2String(parseInt(vessel.DEVICE, 10))
              },
              {
                "path": "navigation.gnss.methodQuality",
                "value": parseInt(vessel.PAC, 10)
              },
              {
                "path": "navigation.destination",
                "value": (vessel.DEST != "") ? vessel.DEST : null,
              },
              {
                "path": "navigation.destination.commonName",
                "value": (vessel.DEST != "") ? vessel.DEST : null,
              },
              {
                "path": "navigation.destination.eta",
                "value": vessel.ETA,
              },
              {
                "path": "design.aisShipType",
                "value": parseInt(vessel.TYPE, 10),
              },
              {
                "path": "design.draft",
                "value": {
                  "minimum": null,
                  "maximum": null,
                  "canoe": null,
                  "current": draft,
                },
              },
              {
                "path": "design.length",
                "value": length,
              },
              {
                "path": "design.beam",
                "value": parseInt(vessel.C, 10) + parseInt(vessel.D, 10),
              },
            ],
          },
        ],
      };
    });
  } catch(err) {
    throw err;
  }
}

function update(app, plugin, username, bbox, compression) {
  (async () => {
    try {
      let url = new URL("http://data.aishub.net/ws.php");
      let params = url.searchParams;
      params.append("username", username);
      params.append("format", "0");
      
      if (compression != null) {
        params.append("compression", compression.toString());
      }
      if (bbox != null) {
        params.append("latmin", bbox.minlat.toString());
        params.append("latmax", bbox.maxlat.toString());
        params.append("lonmin", bbox.minlon.toString());
        params.append("lonmax", bbox.maxlon.toString());
      }
      
      let response = await fetch(url)
      let xmlData = response.text();
      let deltas = AisHubXmlToDeltas(xmlData);
      deltas.forEach((delta) => app.handleMessage(plugin.id, delta));
    } catch(err) {
      app.error(err);
    }
  })();
}

module.exports = function (app) {
  let plugin = {};

  plugin.id = 'aishub-reader';
  plugin.name = 'AISHub Reader';
  plugin.description = 'Populates the Signal K server with AIS data from AISHub.';

  schema = {
    title: plugin.name,
    type: 'object',
    required: ["username"],
    properties: {
      username: {
        title: "AISHub API username.",
        type: "string",
        description: "The username to use when doing AISHub API requests. You will have received this if you joined AISHub."
      },
    }
  }

  let theTimeout = null;

  plugin.start = function (options, restartPlugin) {
    // Here we put our plugin logic
    app.debug('Plugin started');

    let username = null;
    let bbox = null;
    let compression = null;
    theTimeout = setInterval(update, 65000, app, plugin, username, bbox, compression);
  };

  plugin.stop = function () {
    // Here we put logic we need when the plugin stops
    theTimeout.stopInterval();
    app.debug('Plugin stopped');
  };

  plugin.schema = {
    // The plugin schema
  };

  return plugin;
};
